/* 1.2.- Algorisme que llegeix un n�mero i compara si �s major que 10. 
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float num;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un n�mero");
		num = sc.nextFloat();
		if (num > 10) {
			System.out.println("El numero introduit es major que 10");

		} else if (num < 10) {
			System.out.println("El numero introduit es menor que 10");
		} else if (num == 10) {
			System.out.println("El numero introduit es igual a 10");
		}
		sc.close();
	}
}
