/*1.18.- Algorisme que llegeix una qualificaci� alfanum�rica i el resultat mostra la qualificaci� num�rica ponderada segons la seg�ent taula:
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex18 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		float nota;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix la teva nota: ");
		nota = sc.nextFloat();

		if (nota <= 1.5 && nota >= 0) {
			System.out.println("Molt Deficient");
		}
		if (nota >= 1.5 && nota < 4) {
			System.out.println("Molt Deficient");
		}
		if (nota >= 4 && nota < 5.5) {
			System.out.println("Insuficient");
		}
		if (nota >= 5.5 && nota < 6.5) {
			System.out.println("Suficient");
		}
		if (nota >= 6.5 && nota < 8) {
			System.out.println("B�");
		}
		if (nota >= 8 && nota < 9.5) {
			System.out.println("Notable");
		}
		if (nota >= 9.5 && nota <= 10) {
			System.out.println("Excel�lent");
		}
		if (nota > 10 || nota < 0) {
			System.out.println("Error");
		}

		sc.close();
	}
}
