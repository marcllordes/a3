/* 1.15.- Algorisme que llegeix dos n�meros en dos variables i intercanvia el seu valor. 
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float a, b, aux;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un n�mero a");
		a = sc.nextFloat();
		System.out.println("Introdueix un n�mero b");
		b = sc.nextFloat();
		aux = b;
		b = a;
		a = aux;

		System.out.println("Numero a: " + a + " Numero b: " + b);

		sc.close();
	}
}