/* 1.11.- Algorisme que llegeix un n�mero i ens diu si �s parell o senar. 
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un n�mero 1: ");
		num1 = sc.nextInt();
		if (num1 % 2 == 0) {
			System.out.println("El n�mero: " + num1 + " �s parell");
		} else {
			System.out.println("El n�mero: " + num1 + " �s senar");
		}
		sc.close();
	}
}