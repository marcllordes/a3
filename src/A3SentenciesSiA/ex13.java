/* 1.13.- Algorisme que llegeix 3 valors corresponents als coeficients A, B, i C d�una equaci� de segon grau i ens diu la soluci� aplicant la f�rmula de resoluci�.
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex13 {
	public static void main(String[] args) {
		int A, B, C, E;
		float X1, X2;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduix n�mero A: ");
		A = sc.nextInt();

		System.out.println("Introdueix n�mero B: ");
		B = sc.nextInt();

		System.out.println("Introdueix n�mero C: ");
		C = sc.nextInt();

		E = (B * B) - (4 * A * C);

		if (E >= 0) {
			if (2 * A != 0) {
				X1 = (-B + ((float) Math.sqrt(E))) / (2 * A);
				X2 = (-B - ((float) Math.sqrt(E))) / (2 * A);
				System.out.println("La primera soluci� �s " + X1);
				System.out.println("La segunda soluci� �s " + X2);
			} else {
				System.out.println("No t� soluci�");
			}
		}
		if (E < 0) {
			System.out.println("No t� soluci�");
		}

		sc.close();
	}
}