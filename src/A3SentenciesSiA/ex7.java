/* 1.7.- Algorisme que llegeix dos n�meros i calcula la suma, la resta, la multiplicaci� i la divisi� reals. 
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float num1, num2, suma, resta, multiplicacio, divisio;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un n�mero 1");
		num1 = sc.nextFloat();
		System.out.println("Introdueix un n�mero 2");
		num2 = sc.nextFloat();

		suma = num1 + num2;
		resta = num1 - num2;
		multiplicacio = num1 * num2;
		divisio = num1 / num2;
		System.out.println("Suma: " + suma);
		System.out.println("Resta: " + resta);
		System.out.println("Muliplicaci�: " + multiplicacio);
		System.out.println("Divisi�: " + divisio);
		sc.close();
	}
}