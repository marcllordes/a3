/*1.20.- Algorisme que llegeix una data d�entrada expressada en dia (1 a 31), mes (1 a 12) i any (amb n�mero) i ens diu la data que hi serem al dia seg�ent. Es suposa que febrer t� sempre 28 dies. 
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int d, m, a;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un dia: (1-31): ");
		d = sc.nextInt();
		System.out.println("Introdueix un mes: (1-12) ");
		m = sc.nextInt();
		System.out.println("Introdueix l'any");
		a = sc.nextInt();
		sc.close();
		if (d == 31 && (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10)) {
			d = 1;
			m++;
			System.out.println(d + "/" + m + "/" + a);
		} else if (d == 31 && m == 12) {
			d = 1;
			m = 1;
			a++;
			System.out.println(d + "/" + m + "/" + a);
		} else if (d == 30 && (m == 4 || m == 6 || m == 9 || m == 11)) {
			d = 1;
			m++;
			System.out.println(d + "/" + m + "/" + a);
		} else if (d == 28 && m == 2) {
			d = 1;
			m++;
			System.out.println(d + "/" + m + "/" + a);
		} else if (d <= 0 || d > 31 || m < 1 || m > 12 || (d >= 29 && m == 2)) {
			System.out.println("Error");
		} else {
			d++;
		}
		sc.close();
	}
}
