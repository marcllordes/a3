/* 1.1.- Algorisme que llegeix un n�mero per teclat, el multiplica per 3 i mostra el resultat
 * Autor: Marc Llord�s
*/

package A3SentenciesSiA;

import java.util.Scanner;

public class ex1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float num;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un n�mero");
		num = sc.nextFloat() * 3;
		System.out.println("El resultat de la multiplicaci� �s: " + num);
		sc.close();

	}

}
