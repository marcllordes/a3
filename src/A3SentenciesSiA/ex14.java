/* 1.14.- Algorisme que llegeix una quantitat per teclat i si �s superior a 500 li fa el 10% de descompte, en cas contrari �nicament li fa el 5%. El resultat cal que ens digui la quantitat descomptada i la quantitat a pagar.
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float quantitat, descompte;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix una quantitat");
		quantitat = sc.nextFloat();
		if (quantitat > 500) {
			descompte = (float) (quantitat * 0.1);
		} else
			descompte = (float) (quantitat * 0.05);
		System.out.println("La quantitat a pagar �s de: " + (quantitat - descompte)+ " �, amb un descompte de " +descompte +" �");
		sc.close();
	}
}