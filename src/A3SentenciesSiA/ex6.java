/* 1.6.- Algorisme que llegeix 3 n�meros i els mostra ordenats de major a menor. 
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex6 {

	public static void main(String[] args) {
		float a, b, c;

		Scanner sc = new Scanner(System.in);

		System.out.println("Introdueix un n�mero:");
		a = sc.nextFloat();
		System.out.println("Introdueix un altre n�mero:");
		b = sc.nextFloat();
		System.out.println("Introdueix l'ultim n�mero:");
		c = sc.nextFloat();

		if (a > b && a > c) {
			if (b > c) {
				System.out.println(+a + " > " + b + " > " + c);
			} else {
				System.out.println(+a + " > " + c + " > " + b);
			}
		} else {

			if (b > c && b > a) {
				if (c > a) {
					System.out.println(+b + " > " + c + " > " + a);
				} else {
					System.out.println(+b + " > " + a + " > " + c);
				}
			} else {
				if (b > a) {
					System.out.println(+c + " > " + b + " > " + a);
				} else {
					System.out.println(+c + " > " + a + " > " + b);
				}
			}
		}
		sc.close();
	}
}