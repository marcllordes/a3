/* 1.12.- Algorisme que llegint una data de naixement i ens digui si pot votar (t� 18 anys o m�s). 
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int dn, mn, an, da, ma, aa;
		Scanner sc = new Scanner(System.in);

		System.out.println("Introdueix el dia de naixement: ");
		dn = sc.nextInt();
		System.out.println("Introdueix el mes de naixement: ");
		mn = sc.nextInt();
		System.out.println("Introdueix l'any de naixement: ");
		an = sc.nextInt();
		System.out.println("Introdueix el dia actual: ");
		da = sc.nextInt();
		System.out.println("Introdueix el mes actual: ");
		ma = sc.nextInt();
		System.out.println("Introdueix l'any actual: ");
		aa = sc.nextInt();
		sc.close();
		if (aa - an > 18) {
			System.out.println("Vost� ja pot votar");
		} else if (aa - an == 18 && ma > mn) {
			System.out.println("Vost� ja pot votar");
		} else if (aa - an == 18 && ma == mn && dn <= da) {
			System.out.println("Vost� ja pot votar");
		} else {
			System.out.println("Vost� no pot votar");
		}
		sc.close();
	}
}