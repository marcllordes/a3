/* 1.10.- Algorisme que llegeix dos n�meros enters positius i diferents i ens diu si el major �s m�ltiple del menor, o el que �s al mateix, que el menor �s divisor del major. 
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1, num2, major, menor;
		Scanner sc = new Scanner(System.in);

		System.out.println("Introdueix un n�mero 1: ");
		num1 = sc.nextInt();

		System.out.println("Introdueix un n�mero 2: ");
		num2 = sc.nextInt();

		if (num1 > num2) {
			major = num1;
			menor = num2;
		} else {
			major = num2;
			menor = num1;
		}
		if (num1 == num2 || (num1 < 0 || num2 < 0)) {
			System.out.println("Error");
		} else if (major % menor == 0) {
			System.out.println("El n�mero: " + major + " �s m�ltiple de: " + menor);
		} else {
			System.out.println("El n�mero: " + major + " no �s m�ltiple de: " + menor);
		}
		sc.close();
	}
}