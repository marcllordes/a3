/* 1.9.- Algorisme que llegeix un n�mero i ens diu si �s m�ltiple de dos o de tres o de cap d�ells.
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un n�mero: ");
		num1 = sc.nextInt();
		if (num1 % 2 == 0) {
			System.out.println("El n�mero: " + num1 + " �s m�ltiple de 2");
		}
		if (num1 % 3 == 0) {
			System.out.println("El n�mero: " + num1 + " �s m�ltiple de 3");
		}
		if (num1 % 2 != 0 && num1 % 3 != 0) {
			System.out.println("El n�mero: " + num1 + " �s un n�mero prim");
		}

		sc.close();
	}
}