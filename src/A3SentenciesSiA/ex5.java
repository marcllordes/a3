/* 1.5.- Algorisme que llegeix 3 n�meros i digui quin �s el major.  
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float num1, num2, num3;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un n�mero 1");
		num1 = sc.nextFloat();
		System.out.println("Introdueix un n�mero 2");
		num2 = sc.nextFloat();
		System.out.println("Introdueix un n�mero 3");
		num3 = sc.nextFloat();

		if (num1 > num2 && num1 > num3) {
			System.out.println("El numero m�s gran �s el n�mero 1");

		} else if (num2 > num1 && num2 > num3) {
			System.out.println("El n�mero m�s gran �s el n�mero 2");
		} else if (num3 > num1 && num3 > num2) {
			System.out.println("El n�mero m�s gran �s el n�mero 3");

		} else if (num1 == num2 || num2 == num3 || num1 == num3) {
			System.out.println("Hi han numeros amb el mateix valor");

		}
		sc.close();

	}
}
