/* 1.8.- Algorisme que calcula la superf�cie d�un rectangle.
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float num1, num2, superficie;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix costat A");
		num1 = sc.nextFloat();
		System.out.println("Introdueix costat B");
		num2 = sc.nextFloat();
		superficie = num1 * num2;
		System.out.println("La superficie del rectangle �s de : " + superficie);
		sc.close();
	}
}