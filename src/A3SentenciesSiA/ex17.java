/* 1.17.- Algorisme que llegeix el preu final pagat per un producte i el seu preu de tarifa i ens mostra el percentatge de descompte que li hem aplicat. 
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float pf, pdt, descompte;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix el preu final");
		pf = sc.nextFloat();
		System.out.println("Introdueix el preu de tarifa");
		pdt = sc.nextFloat();
		descompte = pf * 100 / pdt;
		double i = 100 - descompte;
		System.out.println("Hem aplicat un descompte d'un " + i + " %");
		sc.close();
	}
}