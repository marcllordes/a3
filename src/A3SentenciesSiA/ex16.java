/* 1.16.- Algorisme que llegeix una qualificaci� num�rica v�lida i escriu com a resultat la mateixa qualificaci� per� alfab�ticament. 
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int nota;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix la teva nota");
		nota = sc.nextInt();
		String notaalfa = null;
		if (nota == 0) {
			notaalfa = "Zero";
		}
		if (nota == 1) {
			notaalfa = "�";
		}
		if (nota == 2) {
			notaalfa = "Dos";
		}
		if (nota == 3) {
			notaalfa = "Tres";
		}
		if (nota == 4) {
			notaalfa = "Quatre";
		}
		if (nota == 5) {
			notaalfa = "Cinc";
		}
		if (nota == 6) {
			notaalfa = "Sis";
		}
		if (nota == 7) {
			notaalfa = "Set";
		}
		if (nota == 8) {
			notaalfa = "Vuit";
		}
		if (nota == 9) {
			notaalfa = "Nou";
		}
		if (nota == 10) {
			notaalfa = "Deu";
		}
		System.out.println("La nota: " + nota + " equival a  " + notaalfa);

		sc.close();
	}
}
