/*1.19- Algorisme que llegint una data de naixement i com ha resultat ens diu l�hor�scop corresponent.
 * Autor: Marc Llord�s
 */

package A3SentenciesSiA;

import java.util.Scanner;

public class ex19 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int d, m;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix el dia de naixement (1-31): ");
		d = sc.nextInt();
		System.out.println("Introdueix el mes de naixement: (1-12) ");
		m = sc.nextInt();

		if ((d >= 20 && d < 32 && m == 1) && d <= 18 && d > 0 && m == 2) {
			System.out.println("Acuario");
		}
		if ((d >= 19 && d < 32 && m == 2) || d <= 20 && d > 0 && m == 3) {
			System.out.println("Piscis");
		}
		if ((d >= 21 && d < 32 && m == 3) || d <= 19 && d > 0 && m == 4) {
			System.out.println("Aries");
		}
		if ((d >= 20 && d < 32 && m == 4) || d <= 20 && d > 0 && m == 5) {
			System.out.println("Tauro");
		}
		if ((d >= 21 && d < 32 && m == 5) || d <= 20 && d > 0 && m == 6) {
			System.out.println("G�minis");
		}
		if ((d >= 21 && d < 32 && m == 6) || d <= 22 && d > 0 && m == 7) {
			System.out.println("C�ncer");
		}
		if ((d >= 23 && d < 32 && m == 7) || d <= 22 && d > 0 && m == 8) {
			System.out.println("Leo");
		}
		if ((d >= 23 && d < 32 && m == 8) || d <= 22 && d > 0 && m == 9) {
			System.out.println("Virgo");
		}
		if ((d >= 23 && d < 32 && m == 9) || d <= 22 && d > 0 && m == 10) {
			System.out.println("Libra");
		}
		if ((d >= 23 && d < 32 && m == 10) || d <= 21 && d > 0 && m == 11) {
			System.out.println("Escorpio");
		}
		if ((d >= 22 && d < 32 && m == 11) || d <= 21 && d > 0 && m == 12) {
			System.out.println("Sagitario");
		}
		if ((d >= 22 && m == 12) || d <= 19 && d > 0 && m == 1) {
			System.out.println("Capricornio");
		}

		if (d > 31 || m <= 0 || d < 1 || m > 12) {
			System.out.println("Error");

			sc.close();
		}
	}
}
